from TicTacToe.game import Game
from NQueens.NQueens import NQueens

if __name__ == '__main__':
    print("1. Tic Tac Toe")
    print("2. N Queens")

    game = None
    while True:
        user_choice = int(input("Select the number of the game you want to play: "))
        if user_choice == 1:
            game = Game()
            break
        elif user_choice == 2:
            dim = int(input("Insert dimension: "))
            game = NQueens(dim)
            break
        else:
            print("Wrong choice, Try again!")

    game.main_loop()