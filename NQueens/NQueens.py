from NQueens.board import Board
import math
import time

class NQueens:
    def __init__(self, dimension):
        self.board = Board(dimension)
        self.dimension = dimension

    def main_loop(self):
        start_time = time.time()
        while self.board.queens_in_conflict():
            chosen_queen_pos = self.board.select_queen()
            initial_pos = chosen_queen_pos
            possible_positions = self.board.get_possible_positions(chosen_queen_pos)

            min_conflicts_count = math.inf
            min_conflict_pos = (-1, -1)

            for target_pos in possible_positions:
                self.board.set_movement(chosen_queen_pos, target_pos)
                recent_conflicts_count = self.board.count_conflicts(target_pos)
                if recent_conflicts_count < min_conflicts_count and target_pos != initial_pos:
                    min_conflicts_count = recent_conflicts_count
                    min_conflict_pos = target_pos

                # Undo movement
                self.board.set_movement(target_pos, chosen_queen_pos)

            # Move queen to the position with minimum conflicts
            self.board.set_movement(chosen_queen_pos, min_conflict_pos)
        end_time = time.time()
        self.board.draw()
        print(f"Elapsed Time {round(end_time - start_time, 5)}s")







