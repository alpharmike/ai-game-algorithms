from random import randint
from typing import Tuple, List

class Board:
    def __init__(self, dimension: int):
        self.squares = []
        self.dimension = dimension
        self.initialize()

    def initialize(self):
        """
        Initializing a board with provided dimension and randomize the positioning of queens
        :return: None
        """
        for row in range(self.dimension):
            self.squares.append(self.dimension * [0])
            random_position = randint(0, self.dimension - 1)
            self.squares[row][random_position] = 1

    def get_queen_positions(self) -> list:
        """
        Get all positions where queens are in
        :return: List of queen positions
        """
        positions = []
        for row in range(self.dimension):
            for col in range(self.dimension):
                if self.squares[row][col] == 1:
                    positions.append((row, col))

        return positions

    def queens_in_conflict(self) -> bool:
        positions = self.get_queen_positions()
        result = False
        for curr_pos in positions:
            if self.single_queen_in_conflict(curr_pos, positions):
                return True

        return result

    def single_queen_in_conflict(self, position: Tuple[int, int], all_positions: List[Tuple[int, int]]) -> bool:
        result = False

        for curr_pos in all_positions:
            if curr_pos != position:
                # Check for Vertical Conflict
                if curr_pos[1] == position[1]:
                    result = True

                # Check for Horizontal Conflict
                elif curr_pos[0] == position[0]:
                    result = True

                # Check for Diagonal Conflict
                elif abs(curr_pos[0] - position[0]) == abs(curr_pos[1] - position[1]):
                    result = True

        return result

    def has_conflict(self, first_pos: Tuple[int, int], second_pos: Tuple[int, int]) -> bool:
        result = False
        if first_pos[1] == second_pos[1]:
            result = True

        # Check for Horizontal Conflict
        elif first_pos[0] == second_pos[0]:
            result = True

        # Check for Diagonal Conflict
        elif abs(first_pos[0] - second_pos[0]) == abs(first_pos[1] - second_pos[1]):
            result = True

        return result

    def count_conflicts(self, position: Tuple[int, int]) -> int:
        count = 0
        for queen_pos in self.get_queen_positions():
            if queen_pos != position and self.has_conflict(position, queen_pos):
                count += 1

        return count

    def select_queen(self) -> Tuple[int, int]:
        queen_positions = self.get_queen_positions()
        rand_pos = randint(0, self.dimension - 1)
        return queen_positions[rand_pos]

    def get_possible_positions(self, curr_pos: Tuple[int, int]) -> List[Tuple[int, int]]:
        """
        Available movements within the current queen position's column
        """
        possible_positions = []
        for col in range(self.dimension):
            possible_positions.append((curr_pos[0], col))

        return possible_positions

    def set_movement(self, curr_pos: Tuple[int, int], target_pos: Tuple[int, int]):
        self.squares[curr_pos[0]][curr_pos[1]] = 0
        self.squares[target_pos[0]][target_pos[1]] = 1


    def draw(self):
        for row in self.squares:
            for col in row:
                print(f"{col}| ", end='')
            print()
        print()