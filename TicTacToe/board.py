from typing import List


class Board:
    def __init__(self, human: str, ai: str):
        self.tiles: List[List[str]] = []
        self.human: str = human
        self.ai = ai
        self.initialize()

    def initialize(self):
        """
        Initialize board with empty tiles
        :return: None
        """
        self.tiles = [['E', 'E', 'E'] for i in range(3)]

    def render(self):
        """
        Draw the board tile values so far
        :return: None
        """
        for row in self.tiles:
            for col in row:
                print(f"{col} ", end=" ")
            print()
        print()

    def check_validity(self, row: int, col: int):
        """
        :param row: The relevant row a player has chosen to fill
        :param col: The relevant column of a row a player has chosen to fill
        :return: True -> Valid Assignment / False -> Wrong Assignment
        """
        return row in range(0, 3) and col in range(0, 3) and self.tiles[row][col] == 'E'

    def check_for_end(self):
        """
        Check whether one of the players has won
        Checks for Horizontal Win
        Checks for Vertical Win
        Checks for Diagonal Win
        :return: The winner sign of there is a winner, else None
        """

        win_state = [
            [self.tiles[0][0], self.tiles[0][1], self.tiles[0][2]],  # Horizontal
            [self.tiles[1][0], self.tiles[1][1], self.tiles[1][2]],  # Horizontal
            [self.tiles[2][0], self.tiles[2][1], self.tiles[2][2]],  # Horizontal
            [self.tiles[0][0], self.tiles[1][0], self.tiles[2][0]],  # Vertical
            [self.tiles[0][1], self.tiles[1][1], self.tiles[2][1]],  # Vertical
            [self.tiles[0][2], self.tiles[1][2], self.tiles[2][2]],  # Vertical
            [self.tiles[0][0], self.tiles[1][1], self.tiles[2][2]],  # Diagonal
            [self.tiles[2][0], self.tiles[1][1], self.tiles[0][2]],  # Diagonal
        ]

        human_win = [self.human for i in range(3)]
        ai_win = [self.ai for i in range(3)]

        if human_win in win_state:
            return self.human
        elif ai_win in win_state:
            return self.ai
        elif self.check_for_complete():
            # Tie State
            return 'E'

        # Game has not finishde yet
        return None

    def check_for_complete(self):
        for row in self.tiles:
            for col in row:
                if col == 'E':
                    return False

        return True

    def is_empty(self, row, col):
        return self.tiles[row][col] == 'E'

    def commit_movement(self, row, col, player_sign):
        self.tiles[row][col] = player_sign
