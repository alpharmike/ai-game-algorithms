from TicTacToe.board import Board
import math
from enum import Enum
from random import choice


class SideType(Enum):
    MINIMIZER = -1
    MAXIMIZER = 1


class Game:
    def __init__(self):
        self.over = False
        self.turn: str = ''
        self.board = None
        self.initialize()

    def initialize(self):
        """
        Initialize the game board
        :return: None
        """
        human_sign = self.prompt_sign()
        ai_sign = 'X' if human_sign == 'O' else 'O'
        self.board = Board(human=human_sign, ai=ai_sign)
        self.turn = choice([human_sign, ai_sign])

    def prompt_sign(self):
        """
        Ask player for own sign
        :return: Sign of human side
        """
        choices = ['X', 'O']
        while True:
            human_sign = input("Choose your sign (X/O): ")
            human_sign = human_sign.upper()
            if human_sign in choices:
                return human_sign
            else:
                print("Invalid choice! Try again")

    def minimax(self, side_type: SideType, alpha, beta):
        """
        Implements the minimax algorithm which tries to maximize chance of winning for the calling side and minimize for
        the other side
        """

        game_result = self.board.check_for_end()
        if game_result == self.board.human:
            return -1, 0, 0
        elif game_result == self.board.ai:
            return 1, 0, 0
        elif game_result == 'E':
            return 0, 0, 0

        if side_type == SideType.MAXIMIZER:
            score, row, col = self.maximize(alpha, beta)
        else:
            score, row, col = self.minimize(alpha, beta)

        return score, row, col

    def maximize(self, alpha, beta):
        max_score = -math.inf
        selected_row = -1
        selected_col = -1

        game_result = self.board.check_for_end()
        if game_result == self.board.human:
            return -1, 0, 0
        elif game_result == self.board.ai:
            return 1, 0, 0
        elif game_result == 'E':
            return 0, 0, 0

        for row in range(0, 3):
            for col in range(0, 3):
                if self.board.is_empty(row, col):
                    self.board.tiles[row][col] = self.board.ai
                    score, min_row, min_col = self.minimize(alpha, beta)

                    if score > max_score:
                        max_score = score
                        selected_row = row
                        selected_col = col

                    self.board.tiles[row][col] = 'E'

                    if max_score >= beta:
                        return max_score, selected_row, selected_col

                    alpha = max(alpha, max_score)

        return max_score, selected_row, selected_col

    def minimize(self, alpha, beta):
        min_score = math.inf
        selected_row = -1
        selected_col = -1

        game_result = self.board.check_for_end()

        if game_result == self.board.human:
            return -1, 0, 0
        elif game_result == self.board.ai:
            return 1, 0, 0
        elif game_result == 'E':
            return 0, 0, 0

        for row in range(0, 3):
            for col in range(0, 3):
                if self.board.is_empty(row, col):
                    self.board.tiles[row][col] = self.board.human
                    score, max_row, max_col = self.maximize(alpha, beta)
                    if score < min_score:
                        min_score = score
                        selected_row = row
                        selected_col = col

                    self.board.tiles[row][col] = 'E'

                    if min_score <= alpha:
                        return min_score, selected_row, selected_col

                    beta = min(beta, min_score)

        return min_score, selected_row, selected_col

    def prompt_move(self):
        row = int(input("Select the row: "))
        col = int(input("Select the column: "))
        return row, col

    def change_turn(self):
        curr_turn = self.turn
        self.turn = self.board.human if curr_turn == self.board.ai else self.board.ai

    def main_loop(self):
        """
        The core of the game where events happen
        """
        while True:
            self.board.render()
            game_result = self.board.check_for_end()
            if game_result == self.board.human:
                print('Human is the winner!')
                break
            elif game_result == self.board.ai:
                print('AI is the winner!')
                break
            elif game_result == 'E':
                print('The match is a tie')
                break

            if self.turn == self.board.human:
                score, suggested_row, suggested_col = self.minimax(SideType.MINIMIZER, -math.inf, math.inf)
                print(f"Suggested Row: {suggested_row}")
                print(f"Suggested Column: {suggested_col}")

                row, col = self.prompt_move()

                if self.board.check_validity(row, col):
                    self.board.commit_movement(row, col, self.board.human)

                else:
                    print("Invalid choice! Try again")
                    continue
            else:
                score, row, col = self.minimax(SideType.MAXIMIZER, -math.inf, math.inf)
                print(f"AI Pos: {row} | {col}")
                self.board.commit_movement(row, col, self.board.ai)

            self.change_turn()
